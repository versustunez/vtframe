const CHART_MODE = 0,
    GAUGE_MODE = 1,
    PIE_MODE = 2,
    GAPS = 20,
    FULL_GAP = 40;

class VzgraphsRenderer {
    constructor(graph) {
        this.graph = graph;
    }

    draw(renderer, style, frame) {
        let c = frame.config;
        let graph = this.graph;
        if (c.name === "tooltip") {
            this.drawTooltip(renderer, style, frame);
        } else if (c.name === "scale") {
            this.drawScale(renderer, style, frame)
        } else {
            style.clear();
            switch (graph.mode) {
                case CHART_MODE:
                    this.drawChart(renderer, style, frame);
                    break;
                case GAUGE_MODE:
                    this.drawGauge(renderer, style, frame);
                    break;
                case PIE_MODE:
                    this.drawPie(renderer, style, frame);
                    break;
            }
        }
    }

    drawGauge(renderer, style, f) {
        style.clear();
        let width = f.getWidth(),
            height = f.getHeight(),
            radius = Math.min(width, height) / 2,
            centerX = (width / 2),
            centerY = (height / 2) + (radius / 2),
            graph = this.graph,
            d = graph.data,
            value = d[d.length - 1].value,
            norm = VTUtils.normalize(value, graph.max, graph.min);
        style.colorMode('hex').strokeColor('#212121').strokeWeight(graph.config.gauge || 30).fill(false).fillColor(graph.config.textColor || '#fff');
        renderer.gauge(centerX, centerY, radius, 1);
        style.strokeColor(graph.config.color || '#00ad97');
        renderer.gauge(centerX, centerY, radius, norm).setFont(48);
        style.strokeWeight(graph.config.stroke || 1).fill(true);
        let rounded = this.round(value),
            min = this.round(graph.min),
            max = this.round(graph.max);
        let ctx = renderer.canvas.context;
        let textWidth = ctx.measureText(rounded).width;
        renderer.text(String(rounded), (width / 2) - (textWidth / 2), centerY + 32).setFont(24);

        let minWidth = this.round(ctx.measureText(min).width / 2);
        let maxWidth = this.round(ctx.measureText(max).width / 2);
        renderer.text(String(min), centerX - radius - minWidth, centerY + 32);
        renderer.text(String(max), centerX + radius - maxWidth, centerY + 32);
    }

    drawChart(renderer, style, f) {
        let graph = this.graph,
            data = graph.data,
            threshold = graph.config.threshold || 0,
            lastDraw = 0;
        style.colorMode('hex').strokeColor(graph.config.color || '#26ff48').strokeWeight(1);
        for (let i = 0; i < data.length; i++) {
            let d = data[i];
            if (lastDraw === 0 || Math.abs(data[lastDraw].value - d.value) > threshold || data.length - 1 === i) {
                renderer.fromTo(data[lastDraw].x, data[lastDraw].y, d.x, d.y);
                lastDraw = i;
            }
        }
    }

    drawScale(renderer, style, f) {
        let graph = this.graph;
        let ticks = graph.config.ticks || 10;
        let graphHeight = f.getHeight() - FULL_GAP;
        let graphWidth = f.getWidth() - FULL_GAP,
            ctx = style.context;
        style.clear().strokeWeight(.5);
        renderer.setFont(8);
        if (graph.mode === CHART_MODE) {
            let yOff = graphHeight + GAPS,
                nr = graph.min,
                range = (graph.max - graph.min) / ticks,
                offsetPlus = (graphHeight / ticks);
            let color = 80;
            style.colorMode('rgba').strokeColor(color, color, color, 1).colorMode('hex').fillColor(graph.config.fillColor || '#ffffff');
            for (let i = 0; i <= ticks; i++) {
                let t = String(this.round(nr));
                let x = Math.abs(FULL_GAP - ctx.measureText(t).width) / 2;
                renderer.text(t, x, yOff + 2);
                renderer.fromTo(FULL_GAP, yOff, graphWidth, yOff);
                yOff -= offsetPlus;
                nr += range;
            }
            let fm = FULL_GAP,
                hm = GAPS;
            renderer.fromTo(fm, hm, fm, graphHeight + hm);
            renderer.fromTo(graphWidth, hm, graphWidth, graphHeight + hm);
        }
        let label = graph.label,
            d = graph.data,
            value = 'N/A';
        if(d.length > 0) {
            value = this.round(d[d.length - 1].value);
        }
        let rounded = value + " | Min: " + this.round(graph.real.min) + " | Max: " + this.round(graph.real.max),
            textOff = f.getWidth() / 2;
        renderer.setFont(16);
        let labelWidth = ctx.measureText(label).width;
        let textWidth = ctx.measureText(rounded).width;
        renderer
            .text(label, textOff - (labelWidth / 2), 16)
            .text(rounded, textOff - (textWidth / 2), f.getHeight() - 4);
    }

    drawTooltip(renderer, style, f) {
        let graph = this.graph,
            ctx = style.context,
            tip = graph.tooltip;
        style.clear().colorMode('rgba').alpha(true).fillColor(33, 33, 33, .7).fill(true).strokeWeight(.2);
        renderer.setFont(13);
        if (graph.mode === CHART_MODE && tip !== -1) {
            let w = this.round(ctx.measureText(graph.data[tip.p].title).width) + 10,
                h = 36,
                x = tip.x;
            if (graph.mouse.x > f.getWidth() / 2) {
                x -= w;
            }
            renderer.rect(x, graph.mouse.y, w, h);
            style.fillColor(255, 0, 0);
            renderer.point(tip.x, tip.y, 3);
            renderer.fromTo(tip.x, GAPS, tip.x, f.getHeight() - GAPS);
            style.colorMode('hex').fillColor(graph.config.textColor || '#ffffff')
            renderer.text(graph.data[tip.p].title, x + 5, graph.mouse.y + 15)
                .text(graph.data[tip.p].value, x + 5, graph.mouse.y + 31)
            //draw... else let empty
        }
    }

    round(nr) {
        return Math.round(nr * 100) / 100
    }
}

//bae config... is with JSON.parse because its faster for the engine
const vt_graphs_config = JSON.parse('{"limit":100,"color":"#2bcbba","stroke":1.2,"max":10,"min":-10,"avg":false,"ticks":5, "update": 0, "ticks": 10, "space": 3}');

class VTGraph {
    constructor(element, label, config) {
        let self = this;
        self.config = {...vt_graphs_config, ...config};
        self.label = label;
        self.el = element;
        self.canvas = {};
        this.mode = config.mode || 0;
        self.createFrame("scale");
        self.createFrame("graph");
        self.createFrame("tooltip");
        self.data = [];
        self.start = -1;
        self.end = -1;
        self.rawData = [];
        self.real = {min: 0, max: 0};
        self.tooltip = -1; //tooltip position...
        self.mouse = {x: 0, y: 0};
        self.min = parseFloat(self.config.min);
        self.max = parseFloat(self.config.max);
        if (window.vt_graphs === undefined) {
            window.vt_graphs = [];
        }
        vt_graphs.push(self);
        self.createListener(self.canvas['tooltip'].canvas)
        self.init();
    }

    async init() {
        let self = this;
        //call prepare method before start...
        if (self.config.prepare) {
            await self.config.prepare(this);
            document.body.dispatchEvent(new CustomEvent('prepared'))
        }
        if (self.config.update > 0) {
            setInterval(this.update.bind(this), self.config.update)
        }
        window.addEventListener('resize', e => {
            for (let c in self.canvas) {
                self.canvas[c].resize(self.el.clientWidth, self.el.clientHeight);
            }
            self.afterUpdate(true);
        })
        await this.update();
    }

    setStartEnd(start, end) {
        this.start = parseInt(start);
        this.end = parseInt(end);
        this.afterUpdate();
    }

    createFrame(name) {
        let el = this.el,
            renderer = new VzgraphsRenderer(this),
            config = {
                draw: renderer.draw.bind(renderer),
                width: el.clientWidth,
                height: el.clientHeight,
                appendTo: el,
                name: name,
                graphsRenderer: renderer
            };
        let f = new VTFrame(config);
        f.engine.loopMode(false);
        f.canvas.addClass('graph-canvas ' + name);
        this.canvas[name] = f;
    }

    createListener(canvas) {
        let self = this;
        canvas.addEventListener('touchmove', e => {
            self.mouse = self.relativeCoords({
                target: e.target,
                clientX: e.touches[0].clientX,
                clientY: e.touches[0].clientY
            });
            self.prepareTooltip(self.data, self);
        })
        canvas.addEventListener('mousemove', e => {
            self.mouse = self.relativeCoords(e)
            self.prepareTooltip(self.data, self);
        });
        canvas.addEventListener('click', e => {
            if (self.mouse.y < GAPS) {
                self.toggleMode();
            }
        });
        canvas.addEventListener('mouseover', e => {
            this.prepareTooltip(self.data, self);
        });
        canvas.addEventListener('touchenter', e => {
            this.prepareTooltip(self.data, self);
        });
        canvas.addEventListener('mouseleave', e => {
            self.tooltip = -1;
            this.canvas["tooltip"].engine.loop()
        });
        canvas.addEventListener('touchleave', e => {
            self.tooltip = -1;
            this.canvas["tooltip"].engine.loop()
        });
    }

    relativeCoords(event) {
        let bounds = event.target.getBoundingClientRect(),
            x = event.clientX - bounds.left,
            y = event.clientY - bounds.top;
        return {x: x, y: y};
    }

    prepareTooltip(d, s) {
        //calculate which tooltip have to be in it.
        let minDist = Infinity,
            point = -1;
        //only working because the x position is preCalculated...
        for (let i = 0; i < d.length; i++) {
            let dist = Math.abs(s.mouse.x - d[i].x)
            if (dist < minDist) {
                point = i;
                minDist = dist;
            }
        }
        if (point === -1) {
            s.tooltip = -1
        } else {
            s.tooltip = {x: d[point].x, y: d[point].y, p: point}
        }
        this.canvas["tooltip"].engine.loop()
    }

    toggleMode() {
        this.mode += 1;
        if (this.mode > 1) {
            this.mode = 0;
        }
        this.canvas["scale"].engine.loop()
        this.canvas["graph"].engine.loop()
        this.canvas["tooltip"].engine.loop()
    }

    async update() {
        let self = this;
        if (self.config.onUpdate) {
            await self.config.onUpdate(self);
        }
        self.afterUpdate();
        document.body.dispatchEvent(new CustomEvent('dataUpdated', {
            detail: self
        }))
    }

    // we pre calculate this and if repaint the scale and the graph
    afterUpdate(resize) {
        let raw = this.rawData,
            length = raw.length - 1,
            limit = this.config.limit,
            start = 0;
        if (this.start === -1 && limit && limit !== -1 && length > limit) {
            start = length - this.config.limit;
            length = this.config.limit;
        }
        if (this.start > -1 && this.end > -1) {
            if (this.end > length) {
                this.end = length;
            }
            start = this.start;
            length = this.end - start;
        }
        let t = this.findMinMax(start, length),
            reDrawScale = false;
        if (t.min !== this.min || t.max !== this.max || resize) {
            reDrawScale = true;
        }
        this.min = t.min;
        this.max = t.max;
        let d = [],
            s = (this.el.clientWidth - (FULL_GAP * 2)) / length,
            x = FULL_GAP,
            h = this.el.clientHeight - GAPS;
        for (let i = 0; i <= length; i++) {
            d[i] = {
                value: raw[i + start].value,
                title: raw[i + start].title,
                x: x,
                y: VTUtils.map(raw[i + start].value, this.min, this.max, h, GAPS)
            }
            x += s;
        }
        this.data = d;
        if (reDrawScale) {
            this.canvas["scale"].engine.loop()
        }
        this.canvas["graph"].engine.loop()
    }

    findMinMax(start, end) {
        let t = {
                min: Infinity,
                max: -Infinity
            },
            raw = this.rawData;

        for (let i = start; i <= start + end; i++) {
            let d = raw[i].value;
            if (d < t.min) {
                t.min = d;
            }
            if (d > t.max) {
                t.max = d;
            }
        }
        this.real = {min: t.min, max: t.max};
        t.max = Math.ceil(t.max + this.config.space);
        t.min = Math.floor(t.min - this.config.space);
        return t;
    }
}