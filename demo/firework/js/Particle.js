function Particle(pos, rgb, frame) {
    this.frame = frame;
    this.pos = new VTVector(pos.x + VTUtils.random(-2, 2), pos.y + VTUtils.random(-2, 2));
    this.vel = VTVector.createRandom(2,2);
    this.vel.mult(VTUtils.random(0.2, 4));
    this.acc = new VTVector(0, 0);
    this.rgb = rgb || getColor();
    this.life = lifeSpan(true);
    this.scale = getScale(frame.getHeight());

    this.applyForce = function () {
        this.acc.x += VTUtils.random(-0.04, 0.04);
    };

    this.update = function () {
        this.vel.add(this.acc);
        this.pos.add(this.vel);
        this.acc.mult(0);
        this.life.value -= this.life.ttl / this.scale;

        return this.life.value < 2;
    };

    this.show = function () {
        let vtc = this.frame.style;
        let draw = this.frame.renderer;
        vtc.fillColor(this.rgb.r, this.rgb.g, this.rgb.b, VTUtils.normalize(this.life.value, 255, 0));
        draw.point(this.pos.x, this.pos.y, this.life.weight * this.scale);
    }
}