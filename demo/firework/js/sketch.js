let force = null;
let fireworks = [];
let maxVector = 0;
let maxFireworks = VTUtils.random(innerWidth / 100);

let vtFrameConfig = {
    mode: '2d',
    preload: function () {
        this.assets.load('sound/explode.mp3', VTLOADER_AUDIO, 'explode', false);
    },
    setup: function () {
        let frame = this;
        let style = this.style;

        let width = frame.getWidth();
        let height = frame.getHeight();

        fireworks.push(new Firework(VTUtils.random(10, width - 10), height, frame));

        force = new VTVector(VTUtils.random(-0.02, 0.02), 0.2);
        maxVector = -(findMaxVelocity(height, 60));

        style.alpha(true).stroke(false).fill(true);
    },
    draw: function () {
        let frame = this,
            style = this.style,
            width = frame.getWidth(),
            height = frame.getHeight();

        style.fillColor(0, 0, 0, .5).background();
        for (let i = 0; i < fireworks.length; i++) {
            let firework = fireworks[i];
            if (firework.life.startTick !== 0) {
                firework.updateTick();
                continue;
            }
            firework.applyForce(force);
            firework.update();
            firework.show();

            if (firework.isDead()) {
                fireworks.splice(i, 1);
            }
        }

        let ran = VTUtils.random();
        if (ran < 0.1 && fireworks.length < maxFireworks) {
            fireworks.push(new Firework(VTUtils.random(width), height, frame));
        }

        if (ran < 0.002) {
            force = new VTVector(VTUtils.random(-0.02, 0.02), 0.2);
            maxFireworks = VTUtils.random(width / 100);
        }

    }
};
new VTFrame(vtFrameConfig).run();