// the rocket is always white and than it light in nice colors <3
function Firework(x, y, frame) {
    this.frame = frame;
    this.height = frame.getHeight();
    this.width = frame.getWidth();
    this.pos = new VTVector(x, y);
    this.vel = new VTVector(0, VTUtils.random(maxVector / 1.5, maxVector));
    this.acc = new VTVector(0, 0);
    this.particles = [];
    this.exploded = false;
    this.life = lifeSpan();
    this.scale = getScale(this.height);
    this.ae = explodeConfig();

    this.applyForce = function (force) {
        if (this.exploded) {
            for (let i = 0; i < this.particles.length; i++) {
                this.particles[i].applyForce(force);
            }
        } else {
            this.acc.add(force);
            if (this.vel.y > maxVector / 4) {
                this.acc.mult(0.3);
            }
        }
    };

    this.update = function () {
        if (!this.exploded) {
            this.vel.add(this.acc);
            this.pos.add(this.vel);
            this.acc.mult(0);

            if (this.vel.y > -1) {
                this.life.value -= this.life.ttl;
            }
            if (this.vel.y >= this.life.ttl) {
                this.explode();
            }

        } else {
            for (let i = 0; i < this.particles.length; i++) {
                if (this.particles[i].update()) {
                    this.particles.splice(i, 1);
                }
            }
        }
    };

    this.show = function () {
        let vtc = this.frame.style;
        let draw = this.frame.renderer;

        if (!this.exploded) {
            vtc.fillColor(255, 255, 255, VTUtils.normalize(this.life.value, 255, 0));
            draw.point(this.pos.x, this.pos.y, this.scale);
        } else {
            for (let i = 0; i < this.particles.length; i++) {
                this.particles[i].show();
            }
        }
    };

    this.explode = function () {
        this.exploded = true;

        //init some particles that force in all spaces :D
        for (let i = 0; i < this.life.pCount; i++) {
            this.particles.push(createParticle(this.pos, this.ae, this.frame));
        }

        let audio = this.frame.assets.getResource('explode').object.cloneNode(true);
        audio.play();
        audio = null;
    };

    this.isDead = function () {
        return this.exploded && this.particles.length < 1 || this.outOfScreen();
    };

    this.outOfScreen = function () {
        return this.pos.x < 0 || this.pos.x > this.width || this.pos.y < 0 || this.pos.y > this.height;
    };

    this.updateTick = function () {
        this.life.startTick = this.life.startTick - 0.25;
    }

}