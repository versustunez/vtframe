let vtFrame = new VTFrame(innerWidth, innerHeight, '2d', false);
let pos = new VTVector(0, 0);
let sprite;
vtFrame.preload = function () {
    this.assets.load('assets/Faelon.png', VTLOADER_IMAGE, 'FaelonSprite', true);
    this.assets.load('assets/Albert.png', VTLOADER_IMAGE, 'AlbertSprite', true);
    this.assets.load('assets/Doggy.png', VTLOADER_IMAGE, 'DoggySprite', true);
    this.assets.load('assets/Kitty.png', VTLOADER_IMAGE, 'KittySprite', true);
};

vtFrame.engine.setup = function () {
    let frame = this,
        style = frame.style,
        assets = frame.assets;

    style.fillColor(0, 0, 0, 1).background().strokeColor(255, 255, 255, 1).alpha(true);
    sprite = new VTSprite(assets.getResource('AlbertSprite'), 16, 16);
    sprite.create();
};


vtFrame.engine.draw = function () {
    let frame = this,
        style = frame.style,
        width = frame.getWidth(),
        height = frame.getHeight(),
        renderer = frame.renderer;

    style.background();

    renderer.circle(width / 2, height / 2, 50);
    renderer.imageSprite(sprite.getFrame(), pos.x, pos.y, 64, 64);

};


vtFrame.engine.keyPressed = function (event) {
    let key = event.key;
    if (key === 'ArrowRight') {
        sprite.nextFrame();
    }
    if(key === 'ArrowLeft') {
        sprite.previousFrame();
    }
};

vtFrame.run();