class VTVector {
    constructor(x, y, z) {
        this.x = x || 0;
        this.y = y || 0;
        this.z = z || 0;
    }

    //helper
    static createRandom(x, y, z) {
        x = x || 1;
        y = y || 1;
        z = z || 0;
        return new VTVector(VTUtils.random(-x, x), VTUtils.random(-y, y), VTUtils.random(-z, z));
    }

    mult(times) {
        this.x *= times;
        this.y *= times;
        this.z *= times;
    }

    set(vector) {
        this.x = vector.x;
        this.y = vector.y;
        this.z = vector.z;
    }

    add(vector) {
        this.x = this.x + vector.x;
        this.y = this.y + vector.y;
        this.z = this.z + vector.z;
    }

    addXYZ(x, y, z) {
        this.x = x || 0;
        this.y = y || 0;
        this.z = z || 0;
        this.x = this.x + x;
        this.y = this.y + y;
        this.z = this.z + z;
    }

    clone() {
        return new VTVector(this.x, this.y, this.z);
    }
}