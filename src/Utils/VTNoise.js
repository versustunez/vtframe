class VTNoise {
    constructor(seed) {
        seed = seed || Math.random();
        this.seed = VTUtils.random(-seed, seed);

        this.lastValue = 0;
    }

    static noise(value, seed) {
        seed = seed || Math.random();
        seed = VTUtils.random(-seed, seed);
        return value + seed;
    }

    logicNoise(value) {
        let randomValue;
        if (this.lastValue < 0) {
            randomValue = VTUtils.random(this.seed);
        } else {
            randomValue = VTUtils.random(-this.seed, 0);
        }

        this.lastValue = randomValue;
        return value + randomValue;
    }

}