class VTStyle {
    constructor(canvas, config) {
        this.canvas = canvas;
        this.context = canvas.getContext(config.mode);
        this.init();
    }

    init() {
        this.config = {
            stroke: true,
            fill: false,
            colorMode: 'rgb',
            alpha: false
        }
    }

    colorMode(mode) {
        mode = mode || 'rgb';
        this.config.colorMode = mode;
        return this;
    }

    fillColor(a, b, c, d) {
        let context = this.context;
        let mode = this.config.colorMode;
        context.fillStyle = VTUtils.getColor(a, b, c, d, mode, this.config.alpha);
        return this;
    }

    strokeColor(a, b, c, d) {
        let context = this.context;
        let mode = this.config.colorMode;
        context.strokeStyle = VTUtils.getColor(a, b, c, d, mode, this.config.alpha);
        return this;
    }

    alpha(boolean) {
        this.config.alpha = boolean;
        return this;
    }

    strokeWeight(weight) {
        this.context.lineWidth = weight;
        return this;
    }

    stroke(boolean) {
        this.config.stroke = boolean;
        return this;
    }

    fill(boolean) {
        this.config.fill = boolean;
        return this;
    }

    draw() {
        let context = this.context;
        let config = this.config;
        if (config.stroke)
            context.stroke();
        if (config.fill)
            context.fill();
        return this;
    }

    background() {
        let context = this.context;
        context.beginPath();
        context.fillRect(0, 0, this.canvas.width, this.canvas.height);
        context.closePath();
        return this;
    }

    clear() {
        let context = this.context;
        context.beginPath();
        context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        context.closePath();
        return this;
    }

    getContext() {
        return this.context;
    }
}