window._vtfc = 0;

class VTFrame {
    constructor(config) {
        this.id = window._vtfc++;
        console.time("VTFrame_" + this.id);
        config = VTFrame.getDefaultConfig(config);
        this.config = config;
        this.style = new VTStyle(VTFrame.createCanvas(config.width, config.height, config), config);
        this.renderer = new VTRenderer(this.style, config);
        this.engine = new VTEngine(this, config);
        this.assets = new VTLoader();
        this.eventSet = false;
        return this;
    }

    static createCanvas(width, height, config) {
        width = width || 800;
        height = height || 600;
        let c = document.createElement('canvas');
        c.height = height;
        c.width = width;
        if (!config['appendTo'] instanceof Node) {
            config['appendTo'] = document.body;
        }
        config['appendTo'].appendChild(c);
        return c;
    }

    static getDefaultConfig(config) {
        this.getDefault('mode', '2d', config);
        this.getDefault('height', innerHeight, config);
        this.getDefault('width', innerWidth, config);
        this.getDefault('appendTo', document.body, config);
        this.getDefault('draw', 'f', config);
        this.getDefault('setup', 'f', config);
        this.getDefault('preload', 'f', config);
        this.getDefault('keyPressed', 'f', config);
        this.getDefault('keyDown', 'f', config);
        this.getDefault('keyUp', 'f', config);
        this.getDefault('mouseDown', 'f', config);
        this.getDefault('mouseUp', 'f', config);
        this.getDefault('mouseMove', 'f', config);
        return config;
    }

    static getDefault(parameter, callback, config) {
        if (callback === 'f') {
            callback = function () {
            };
        }
        config[parameter] = config[parameter] || callback;
    }

    run() {
        this.config.preload.call(this);
        if (this.assets.preloading()) {
            if (!this.eventSet) {
                window.addEventListener(VTLOADER_EVENT, this.engine.start.bind(this.engine));
                this.eventSet = true;
            }
        } else {
            this.engine.start.call(this.engine);
        }
    }

    getHeight() {
        return this.config.height;
    }

    getWidth() {
        return this.config.width;
    }

}
