let VTLOADER_AUDIO = 'audio';
let VTLOADER_IMAGE = 'image';
let VTLOADER_EVENT = 'VTLoader_Preload';

class VTLoader {
    constructor() {
        this.resources = {};
        this.count = 0;
        this.preload = {
            count: 0,
            finished: 0
        }
    }

    static errored(event) {
        console.error(event);
    }

    load(url, type, name, preload, event) {
        event = event || 'load';
        name = name || 'VTR' + this.count++;
        console.time(name);
        preload = preload === true;

        if (this.resources.hasOwnProperty(name)) {
            console.error(`Resource with the name: ${name} already exists`);
            return;
        }

        let resource;
        if (type === 'audio')
            resource = new Audio();
        else if (type === 'image')
            resource = new Image();
        else {
            console.error(`Type: ${type} is not a valid type`);
            return;
        }


        this.resources[name] = {
            object: resource,
            name: name,
            preload: preload,
            loaded: false
        };

        if (preload) {
            this.preload.count++;
        }

        resource.addEventListener(event, this.success.bind(this));
        resource.onerror = VTLoader.errored;
        resource._vtname = name;
        resource.src = url;
    }

    success(event) {
        let target = event.target;
        let resource = this.resources[target._vtname];
        console.timeEnd(target._vtname);
        resource.loaded = true;
        if (resource.preload) {
            this.preload.finished++;
            if (!this.preloading()) {
                console.log("VTLoader", "Preloading finished");
                dispatchEvent(new CustomEvent(VTLOADER_EVENT, {bubbles: true}))
            }
        }
    }

    getResource(name) {
        return this.resources[name];
    }

    preloading() {
        let stillLoading = false;
        let keys = Object.keys(this.resources);
        for (let key of keys) {
            let resource = this.resources[key];
            if (resource.preload && !resource.loaded) {
                stillLoading = true;
                break;
            }
        }
        return stillLoading;
    }
}